import React from 'react'
import { BrowserRouter, Route} from 'react-router-dom'
import SideBar from './components/SideBar'
import Book1 from './components/sample_book-1'
import Book2 from './components/sample_book-2'
import Home from './components/Home'

const App = () => {
	return (
		<BrowserRouter>
			<SideBar />
				<Route exact path='/' component={Home} />
				<Route path='/book-1' component={Book1} />
				<Route path='/book-2' component={Book2} />
		</BrowserRouter>
	)
}

export default App;
