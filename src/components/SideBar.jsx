import React from 'react'
import 'react-pro-sidebar/dist/css/styles.css';
import { Link } from 'react-router-dom'
import SideNav, {NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import '../style.css'

const SideBar = () => {
    return (
        <SideNav className="sidenav"
        onSelect={(selected) => {
            // Add your code here
        }}
    >
        <SideNav.Toggle />
        <SideNav.Nav defaultSelected="home" className="text">
            <NavItem eventKey="home">
                <NavIcon>
                    <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                    <Link to='/'>
                        Home
                    </Link>
                </NavText>
            </NavItem>
            <NavItem eventKey="book1"> 
                <NavIcon>
                    <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                    <Link to='/book-1'>
                        Sample Book 1
                    </Link>
                </NavText>
            </NavItem>
            <NavItem eventKey="book2">
                <NavIcon>
                    <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                    <Link to='/book-2'>
                        TEST
                    </Link>
                </NavText>
            </NavItem>
        </SideNav.Nav>
    </SideNav>
    )
}

export default SideBar